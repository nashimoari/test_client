<?php

namespace Nashimoari\ExampleClient;

use Nashimoari\ExampleClient\Actions\Comments;

class ExampleClient
{
    protected const API_HOST = 'http://example.com';

    /** @var ExampleApiRequest */
    private $apiRequest;

    /** @var Comments */
    private $comments;

    public function __construct()
    {
        $this->apiRequest = new ExampleApiRequest(self::API_HOST);
    }

    /**
     * @return Comments
     */
    public function comments(): Comments
    {
        if (!$this->comments) {
            $this->comments = new Comments($this->apiRequest);
        }

        return $this->comments;
    }

}
