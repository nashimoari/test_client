<?php


namespace Nashimoari\ExampleClient;


class ExampleApiRequest
{
    private $host;

    public function __construct($host)
    {
        $this->host = $host;
    }

    public function get($route): array
    {
        $curl = curl_init("{$this->host}/$route");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        if (!$result) {
            throw new \Exception('get request failed');
        }

        return json_decode($result, 1);
    }

    public function post($route, $params): array
    {
        $jsonParams = json_encode($params);

        $curl = curl_init("{$this->host}/$route");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonParams);
        $result = curl_exec($curl);
        curl_close($curl);

        if (!$result) {
            throw new \Exception('post request failed');
        }

        return json_decode($result, 1);
    }

    public function postBool($route, $params): bool
    {
        $jsonParams = json_encode($params);

        $curl = curl_init("{$this->host}/$route");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonParams);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public function update($route, $id, $params): bool
    {
        $jsonParams = json_encode($params);

        $curl = curl_init("{$this->host}/$route/{$id}");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonParams);
        $result = curl_exec($curl);
        curl_close($curl);

        if (!$result) {
            throw new \Exception('update request failed');
        }

        return $result;
    }
}
