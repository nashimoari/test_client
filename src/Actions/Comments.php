<?php


namespace Nashimoari\ExampleClient\Actions;

use Nashimoari\ExampleClient\ExampleApiRequest;

class Comments
{
    /**
     * @var ExampleApiRequest
     */
    private $apiRequest;

    public function __construct(ExampleApiRequest $apiRequest)
    {
        $this->apiRequest = $apiRequest;
    }


    /**
     * Возвращает список комментариев
     * Предположим что данные возвращаются в JSON
     */
    public function getComments()
    {
        return $this->apiRequest->get('comments');
    }

    /**
     * Добавить комментарий
     */
    public function addComment($name, $text): array
    {
        return $this->apiRequest->post('comments', ['name' => $name, 'text' => $text]);
    }

    /**
     * По идентификатору комментария обновляет поля, которые были в в запросе
     */
    public function updateComment($id, $name, $text)
    {
        return $this->apiRequest->update('comments', $id, ['name' => $name, 'text' => $text]);
    }

}
